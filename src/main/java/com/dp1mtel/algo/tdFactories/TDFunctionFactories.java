package com.dp1mtel.algo.tdFactories;

import com.dp1mtel.algo.tdfunction.TDFunctionFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class TDFunctionFactories {

    private static final List<TDFunctionFactory> factories = new ArrayList<>();

    private TDFunctionFactories() {
    }

    static {
        factories.add(new DefaultTDFunctionFactory());
    }

    public static List<TDFunctionFactory> getFactories() {
        return Collections.unmodifiableList(factories);
    }

    public static Optional<TDFunctionFactory> getFactoryByName(String factoryName) {
        if (factoryName == null) {
            return Optional.empty();
        } else {
            return getFactories().stream().
                    filter(f -> factoryName.equals(f.getName())).
                    findAny();
        }
    }
}
