package com.dp1mtel.algo.tdFactories;

import com.dp1mtel.algo.model.Instance;
import com.dp1mtel.algo.tdfunction.TDFunction;
import com.dp1mtel.algo.tdfunction.TDFunctionFactory;

public class DefaultTDFunctionFactory implements TDFunctionFactory {

    @Override
    public String getName() {
        return "DEFAULT";
    }

    @Override
    public TDFunction createTDFunction(Instance instance) {
        double[][] distanceMatrix = instance.distanceMatrix();
        double closingTime = instance.getDepot().getClosingTime();
        return (from, to, startTime) -> {

            if (startTime > closingTime || startTime < 0) {
                return Double.POSITIVE_INFINITY;
            }

            return distanceMatrix[from][to];
        };
    }
}
