package com.dp1mtel.algo.instances;

import com.dp1mtel.algo.instances.solomon.SolomonLoader;
import com.dp1mtel.algo.model.Instance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Instances {
    private static final List<Instance> instances = new ArrayList<>();

    private Instances() {
    }

    static {
        instances.addAll(new SolomonLoader().load());
    }

    public synchronized static List<Instance> getInstances() {
        return Collections.unmodifiableList(instances);
    }

    public static Optional<Instance> getInstanceByName(String instanceName) {
        if (instanceName == null) {
            return Optional.empty();
        } else {
            return getInstances().stream().
                    filter(f -> instanceName.equals(f.getName())).
                    findAny();
        }
    }
}
