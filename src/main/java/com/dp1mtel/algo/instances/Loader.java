package com.dp1mtel.algo.instances;

import com.dp1mtel.algo.model.Instance;

import java.util.List;

public interface Loader {
    List<Instance> load();
}
