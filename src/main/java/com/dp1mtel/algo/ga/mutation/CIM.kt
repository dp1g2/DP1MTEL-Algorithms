package com.dp1mtel.algo.ga.mutation

import com.dp1mtel.algo.ga.Chromosome
import com.dp1mtel.algo.ga.GASolver
import java.util.*

class CIM : Mutation {

    internal var random = Random(GASolver.RANDOM_SEED)

    override fun mutate(chromosome: Chromosome) {

        val length = chromosome.route().size
        val c = random.nextInt(length - 1)

        reverse(chromosome, 0, c)
        reverse(chromosome, c + 1, length - 1)

    }

    private fun reverse(chromosome: Chromosome, iA: Int, iB: Int) {
        var iA = iA
        var iB = iB
        val array = chromosome.route()
        var tmp: Int
        while (iB > iA) {
            tmp = array[iB]
            array[iB] = array[iA]
            array[iA] = tmp
            iB--
            iA++
        }
    }

}