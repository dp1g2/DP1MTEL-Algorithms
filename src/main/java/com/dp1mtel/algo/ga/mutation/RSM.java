package com.dp1mtel.algo.ga.mutation;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.GASolver;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RSM implements Mutation {
    Random random = new Random(GASolver.RANDOM_SEED);

    @Override
    public void mutate(Chromosome chromosome) {
        int length = chromosome.route().length;

        int iA = random.nextInt(length);
        int iB = random.nextInt(length);
        if (iA == iB) {
            iB = (iB + 1) % length;
        }

        reverse(chromosome, iA, iB);
    }

    private void reverse(Chromosome chromosome,  int iA, int iB) {
        int[] array = chromosome.route();
        int tmp;
        while (iB > iA) {
            tmp = array[iB];
            array[iB] = array[iA];
            array[iA] = tmp;
            iB--;
            iA++;
        }
    }
}
