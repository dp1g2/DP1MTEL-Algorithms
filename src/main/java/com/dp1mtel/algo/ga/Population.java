package com.dp1mtel.algo.ga;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Population {
    private final Set<Chromosome> chromosomes = new HashSet<>();

    public Population(Collection<Chromosome> chromosomes) {
        addAll(chromosomes);
    }

    public Set<Chromosome> getChromosomes() {
        return chromosomes;
    }

    public void add(Chromosome chromosome) {
        chromosomes.add(chromosome);
    }

    public void addAll(Collection<Chromosome> chromosomes) {
        this.chromosomes.addAll(chromosomes);
    }

    public boolean remove(Chromosome chromosome) {
        return chromosomes.remove(chromosome);
    }

    public boolean removeAll() {
        chromosomes.clear();
        return true;
    }

    public Optional<Chromosome> getBestChromosome() {
        return chromosomes.stream().max((c1, c2) -> (int) (c1.fitness() - c2.fitness()));
    }

    public Optional<Chromosome> getWorstChromosome() {
        return chromosomes.stream().min((c1, c2) -> (int) (c1.fitness() - c2.fitness()));
    }
}
