package com.dp1mtel.algo.ga.replacement;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.Collection;

public interface Replacement {
    void replace(Population population, Collection<Chromosome> children);
}
