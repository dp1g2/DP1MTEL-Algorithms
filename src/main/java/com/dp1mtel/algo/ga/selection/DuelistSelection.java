package com.dp1mtel.algo.ga.selection;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class DuelistSelection implements Selection {
    private Random random = ThreadLocalRandom.current();

    @Override
    public List<Chromosome> select(Population population, int selectionSize) {
        List<Chromosome> result = new ArrayList<>();
        List<Chromosome> chromosomes = new ArrayList<>(population.getChromosomes());

        while (result.size() < selectionSize) {

            int iA = random.nextInt(chromosomes.size());
            int iB = random.nextInt(chromosomes.size());
            if (iA == iB) {
                iB = (iB + 1) % chromosomes.size();
            }
            Chromosome a = chromosomes.get(iA);
            Chromosome b = chromosomes.get(iB);

            if (a.fitness() > b.fitness()) {
                result.add(a);
            } else {
                result.add(b);
            }
        }
        return result;
    }
}
