package com.dp1mtel.algo.ga.selection;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.Collection;

public interface Selection {
    Collection<Chromosome> select(Population population, int selectionSize);
}
