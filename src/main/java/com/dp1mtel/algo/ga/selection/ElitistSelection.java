package com.dp1mtel.algo.ga.selection;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.*;

public class ElitistSelection implements Selection {

    @Override
    public Collection<Chromosome> select(Population population, int selectionSize) {

        SortedMap<Double, Chromosome> map = new TreeMap<>();
        population.getChromosomes().forEach(c -> map.put(1d / c.fitness(), c));
        List<Chromosome> selectedPopulation = new ArrayList<>(selectionSize);
        for (Chromosome chromosome : map.values()) {
            selectedPopulation.add(chromosome);
            if (--selectionSize == 0) {
                break;
            }
        }
        return selectedPopulation;
    }
}
