package com.dp1mtel.algo.ga;

import com.dp1mtel.algo.model.Instance;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.Solver;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class GASolver implements Solver {

    public static final long RANDOM_SEED = System.nanoTime();

    Random random = new Random(RANDOM_SEED);

    GAOptions options = new GAOptions();

    public GASolver() {
    }

    public GASolver(GAOptions options) {
        this.options = options;
    }

    @Override
    public String getName() {
        return "GA_SOLVER";
    }

    @Override
    public Optional<Solution> solve(Instance instance, TDFunction tdFunction) {

        Population population = PopulationCreator.createPopulation(instance, tdFunction, options);
        Chromosome bestEver = null;
        int round = 0;
        int roundsSinceLastImprovement = 0;
        double bestPopulationFitness = Double.NEGATIVE_INFINITY;
        long executionTime = 0;
        long startTime = System.currentTimeMillis();

        boolean shouldContinue = true;
        while (shouldContinue) {
            List<Chromosome> children = new ArrayList<>();
            List<Chromosome> selection = options.selection().
                    select(population, (int) (options.selectionRate() * population.getChromosomes().size())).
                    stream().
                    map(Chromosome::child).
                    collect(Collectors.toList());

            int length = selection.size();

            for (int i = 0; i < length; i++) {
                int other = random.nextInt(length);
                if (other == i) {
                    other = (other + 1) % length;
                }
                Chromosome child = selection.get(i).cross(selection.get(other));
                if (random.nextDouble() <= options.mutationProbability()) {
                    child.mutate();
                }
                children.add(child);
            }

            options.replacement().replace(population, children);
            Chromosome best = population.getBestChromosome().orElse(null);

            double populationFitness = population.getChromosomes().stream()
                    .mapToDouble(Chromosome::fitness).sum();

            if (best != null && (bestEver == null || bestEver.fitness() < best.fitness())) {
//                System.out.println("updated best chromosome in round " + (options.maxRounds() - round) + " to " + best.fitness + " " + population.getChromosomes().size());
                bestEver = best;
            }

            if (populationFitness > bestPopulationFitness) {
                bestPopulationFitness = populationFitness;
                roundsSinceLastImprovement = 0;
            } else {
                roundsSinceLastImprovement++;
            }
            executionTime = System.currentTimeMillis() - startTime;
            shouldContinue = ++round < options.maxRounds()
                    && roundsSinceLastImprovement < options.maxRoundsWithoutImproving()
                    && executionTime < options.maxExecutionTime();
        }

        if (bestEver == null) {
            return Optional.empty();
        } else {
            return Optional.of(new Solution(instance, tdFunction, bestEver.routes(options.splitter())));
        }

    }

}
