package com.dp1mtel.algo.ga.splitter;

import com.dp1mtel.algo.ga.Chromosome;

import java.util.Collection;

public interface Splitter {
    Collection<int[]> split(Chromosome chromosome);
}
