package com.dp1mtel.algo.ga.crossover;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.ChromosomePair;

public interface Crossover {
    ChromosomePair cross(Chromosome parent1, Chromosome parent2);
}
