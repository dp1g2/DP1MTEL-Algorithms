package com.dp1mtel.algo;

import com.dp1mtel.algo.ga.GASolver;
import com.dp1mtel.algo.instances.Instances;
import com.dp1mtel.algo.model.Instance;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.Solver;
//import com.dp1mtel.algo.tabu.TabuSolver;
import com.dp1mtel.algo.tdFactories.TDFunctionFactories;
import com.dp1mtel.algo.tdfunction.TDFunction;
import com.dp1mtel.algo.tdfunction.TDFunctionFactory;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Instance> instanceList = Instances.getInstances();

        Solver[] solvers = new Solver[] { new GASolver() };
        for (Solver solver : solvers) {
            String solverName = solver.getName();
            System.out.println("Solver: " + solverName);
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("res/" + solverName + "_res.txt")));
                writer.write("Dataset, Tiempo, Distancia\n");
            } catch (Exception ex) {
                System.out.println("Error opening output file");
                System.exit(1);
            }

            int i = 0;
            for (Instance instance : instanceList) {
                if (i > 0) {
                    break;
                }
                i++;
                System.out.println("Working on instance: " + instance.getName());

                TDFunctionFactory tdFunctionFactory = TDFunctionFactories.getFactoryByName("DEFAULT").get();
                TDFunction tdFunction = tdFunctionFactory.createTDFunction(instance);

                long startTime = System.currentTimeMillis();
                Solution solution = solver.solve(instance, tdFunction).orElse(null);
                long executionTime = System.currentTimeMillis() - startTime;

                try {
                    if (solution == null) {
                        writer.write(instance.getName() + ", Solution not found\n");
                        writer.flush();
                        continue;
                    }

                    if (!solution.isValid()) {
                        writer.write(instance.getName() + ", Solution not valid\n");
                        writer.flush();
                        continue;
                    }

                    writer.write(instance.getName() + "," + executionTime + "," + solution.travelTime() + "\n");
                    writer.flush();
                } catch (Exception ex) {
                    System.err.println("Error opening file: " + ex.getMessage());
                    break;
                }
            }

            try {
                writer.close();
            } catch (Exception ex) {}
        }
    }
}