package com.dp1mtel.algo.solver;

import java.util.Optional;

public class SolverResult {
    private final Optional<Solution> solution;
    private final long executionTime;

    public SolverResult(Optional<Solution> solution, long executionTime) {
        this.solution = solution;
        this.executionTime = executionTime;
    }

    public Optional<Solution> getSolution() {
        return solution;
    }

    public long getExecutionTime() {
        return executionTime;
    }
}
