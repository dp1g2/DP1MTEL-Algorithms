package com.dp1mtel.algo.solver;

import com.dp1mtel.algo.model.Instance;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.Optional;

public interface Solver {

    /**
     * Get the name of the solver
     *
     * @return the name
     */
    String getName();

    /**
     * Solve the instance with the time-dependent function
     *
     * @param instance The instance to solve
     * @return the solution
     */
    Optional<Solution> solve(Instance instance, TDFunction tdFunction);
}
