package com.dp1mtel.algo.solver;

import com.dp1mtel.algo.model.Customer;
import com.dp1mtel.algo.model.Instance;
import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    private final Instance instance;
    private final TDFunction tdFunction;
    private final Collection<Route> routes;

    public Solution(Instance instance, TDFunction tdFunction, Collection<Route> routes) {
        this.instance = instance;
        this.tdFunction = tdFunction;
        this.routes = routes;
    }

    /**
     * Returns the instance of the problem solution.
     *
     * @return the instance
     */
    public Instance getInstance() {
        return instance;
    }

    /**
     * Returns the time-dependent function.
     *
     * @return time-dependent function
     */
    public TDFunction getTDFunction() {
        return tdFunction;
    }

    /**
     * Returns the routes of the solution.
     *
     * @return the routes
     */
    public Collection<Route> getRoutes() {
        return Collections.unmodifiableCollection(routes);
    }

    /**
     * Checks the validity of this solution.
     *
     * @return {@code true] if this solution is valid
     */
    public boolean isValid() {
        Set<Customer> customers = new HashSet<>(instance.getCustomers());

        if (getRoutes().size() > instance.getAvailableVehicles()) {
            return false;
        }

        for (Route route : getRoutes()) {
            if (route.getCustomers().stream().mapToDouble(c -> c.getDemand()).sum() > instance.getVehicleCapacity()) {
                return false;
            }
            if (!route.getCustomers().stream().allMatch(customers::contains)) {
                return false;
            }
            route.getCustomers().forEach(customers::remove);
        }

        return customers.isEmpty();
    }

    public double travelTime() {
        return getRoutes().stream().mapToDouble((route) -> route.travelTime(tdFunction)).sum();
    }
}