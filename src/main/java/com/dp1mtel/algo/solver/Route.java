package com.dp1mtel.algo.solver;

import com.dp1mtel.algo.model.Customer;
import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.*;
import java.util.stream.Stream;

public final class Route {

    private final List<Customer> customers = new ArrayList<>();

    public Route(List<Customer> customers) {
        this.customers.addAll(customers);
    }

    /**
     * Returns the list of customers.
     *
     * @return list of customers
     */
    public List<Customer> getCustomers() {
        return Collections.unmodifiableList(customers);
    }

    public int[] toIntArray() {
        return customers.stream().mapToInt(Node::getNumber).toArray();
    }

    public double travelTime(TDFunction tdFunction) {
        double departureTime = 0;
        double totalTravelTime = 0;
        List<Customer> customers = getCustomers();
        int[] route = toIntArray();
        int position = 0;
        int idx = 0;
        for (int customer : route) {
            double travelTime = tdFunction.travelTime(position, customer, departureTime);
            totalTravelTime += travelTime;
            double arrivalTime = departureTime + travelTime;
            position = customer;
            departureTime = Math.max(arrivalTime, customers.get(idx).getReadyTime()) + customers.get(idx).getServiceTime();
            idx++;
        }
        if (route.length != 0) {
            totalTravelTime += tdFunction.travelTime(route[route.length - 1], 0, departureTime);
        }
        return totalTravelTime;
    }

    public Stream<Customer> stream() {
        return customers.stream();
    }
}