package com.dp1mtel.algo.utils;

public class Pair<T, R> {
    private T left;
    private R right;

    public Pair(T left, R right) {
        this.left = left;
        this.right = right;
    }

    public T left() {
        return left;
    }

    public R right() {
        return right;
    }
}
