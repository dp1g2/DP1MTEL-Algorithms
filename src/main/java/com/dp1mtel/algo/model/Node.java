package com.dp1mtel.algo.model;

import java.awt.geom.Point2D;

public class Node implements Numberable, Comparable<Node> {
    private final Point2D position;
    private final int number;
    private final double demand;

    public Node(int number, Point2D position, double demand) {
        this.number = number;
        this.position = position;
        this.demand = demand;
    }

    /**
     * Returns the position of the node
     *
     *  @return the position
     */
    public Point2D getPosition() {
        return position;
    }

    /**
     * Returns the demand of the node
     *
     * @return the demand
     */
    public double getDemand() {
        return demand;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public int hashCode() {
        return 11 * this.getNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (getClass() != o.getClass()) {
            return false;
        }

        return this.getNumber() == ((Node) o).getNumber();
    }

    @Override
    public int compareTo(Node node) {
        return getNumber() - node.getNumber();
    }
}
