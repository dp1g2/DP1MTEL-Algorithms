package com.dp1mtel.algo.model;

import java.awt.geom.Point2D;

public class Customer extends Node {
    private final double readyTime;
    private final double dueTime;
    private final double serviceTime;

    public Customer(int number, Point2D position, double demand, double readyTime, double dueTime, double serviceTime) {
        super(number, position, demand);
        this.readyTime = readyTime;
        this.dueTime = dueTime;
        this.serviceTime = serviceTime;
    }

    public double getReadyTime() {
        return readyTime;
    }

    public double getDueTime() {
        return dueTime;
    }

    public double getServiceTime() {
        return serviceTime;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + this.getNumber();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        return this.getNumber() == other.getNumber();
    }
}
