package com.dp1mtel.algo.model;

public interface Numberable {

    /**
     * Returns the specific number of the object.
     *
     * @return the number
     */
    int getNumber();
}
