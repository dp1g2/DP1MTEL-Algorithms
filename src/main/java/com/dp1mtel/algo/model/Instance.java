package com.dp1mtel.algo.model;

import com.dp1mtel.algo.solver.Route;
import com.dp1mtel.algo.solver.Solution;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface Instance {

    /**
     * Returns the name of the instance.
     *
     * @return name of instance
     */
    String getName();

    /**
     * Returns the depot.
     *
     * @return the depot
     */
    Depot getDepot();

    /**
     * Returns a list of customers.
     *
     * @return list of customers
     */
    List<Customer> getCustomers();

    /**
     * Returns the number of available vehicles.
     *
     * @return the number of vehicles
     */
    int getAvailableVehicles();

    /**
     * Returns the capacity of a single vehicle.
     *
     * @return the capacity
     */
    int getVehicleCapacity();

    /**
     * Calculates a distance matrix of the instance.
     *
     * @return distance matrix
     */
    default double[][] distanceMatrix() {
        int n = getCustomers().size() + 1;
        double[][] matrix = new double[n][n];
        getCustomers().forEach(cA
                        -> getCustomers().forEach(cB
                        -> matrix[cA.getNumber()][cB.getNumber()] = cA.getPosition().distance(cB.getPosition())
                )
        );
        matrix[getDepot().getNumber()][getDepot().getNumber()] = 0;
        getCustomers().forEach(c
                        -> {
                    double distance = getDepot().getPosition().distance(c.getPosition());
                    matrix[getDepot().getNumber()][c.getNumber()] = distance;
                    matrix[c.getNumber()][getDepot().getNumber()] = distance;
                }
        );

        return matrix;
    }
}