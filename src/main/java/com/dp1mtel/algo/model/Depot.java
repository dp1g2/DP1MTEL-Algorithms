package com.dp1mtel.algo.model;

import java.awt.geom.Point2D;

public class Depot extends Node {
    private final double closingTime;

    public Depot(Point2D position, double closingTime) {
        super(0, position, 0);
        this.closingTime = closingTime;
    }

    /**
     * Returns the closing time of the depot.
     *
     * @return the closing time
     */
    public double getClosingTime() {
        return closingTime;
    }
}
