package com.dp1mtel.algo.tdfunction;

import com.dp1mtel.algo.model.Instance;

public interface TDFunctionFactory {

    /**
     * Returns the name of the factoy.
     *
     * @return name
     */
    String getName();

    /**
     * Creates a time-dependent function for the specified instance.
     *
     * @param instance
     * @return time-dependent function
     */
    TDFunction createTDFunction(Instance instance);

}